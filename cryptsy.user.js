// ==UserScript==
// @name        cryptsy enhancements
// @namespace   amDOGE
// @include     http*://*cryptsy.com/markets/view/*
// @include     http*://*coinedup.com/OrderBook*
// @version     1.42
// @grant       none
// ==/UserScript==

(function() {
  var $, CoinedUp, Cryptsy, Main, d;

  d = document;

  $ = function(selector, root) {
    if (root == null) {
      root = d;
    }
    return root.querySelector(selector);
  };

  $.id = function(selector) {
    return d.getElementById(selector);
  };

  $.hasClass = function(el, className) {
    return el.classList.contains(className);
  };

  $.on = function(el, event, handler) {
    return el.addEventListener(event, handler, false);
  };

  $.off = function(el, event, handler) {
    return el.removeEventListener(event, handler, false);
  };

  Cryptsy = {
    name: 'Cryptsy',
    init: function() {
      var audio;
      this.audio = audio = d.createElement('audio');
      audio.src = 'https://btc-e.com/sounds/message.wav';
      this.makeSpendEditable();
      this.copyNetTotal();
      this.updateTab(true);
      return setInterval(this.updateTab, 1000);
    },
    makeSpendEditable: function() {
      var style;
      this.genBalance = $('.genbalance_3');
      this.genBalance.contentEditable = true;
      style = this.genBalance.style;
      style.outline = '0';
      style.border = 'none';
      return $.on(this.genBalance, 'DOMSubtreeModified', this.genBalance.click);
    },
    copyNetTotal: function() {
      var netTotal;
      netTotal = $.id('sellnettotalspan');
      netTotal.style.cursor = 'pointer';
      return $.on(netTotal('click', function() {
        return Cryptsy.genBalance.firstChild.data = this.firstChild.data;
      }));
    },
    updateTab: function(isInit) {
      var current;
      if (Cryptsy.title == null) {
        Cryptsy.title = {
          "default": d.title
        };
      }
      current = dtuserorderslist.fnGetData().length;
      if (!isInit || Cryptsy.title.previousAmount === current) {
        return;
      }
      if (Cryptsy.title.previousAmount > current) {

        /* Doesn't work proper yet. */
        Cryptsy.audio.play();
      }
      Cryptsy.title.previousAmount = current;
      Cryptsy.title.full = "(" + current + ") " + title["default"];
      return d.title = Cryptsy.title.full;
    }
  };

  CoinedUp = {
    name: 'CoinedUp',
    init: function() {
      var balances, base, coin, currencies, el, foundBase, foundCoin, i, market, tabs, _i, _len;
      tabs = $.id('tabs');
      this.sellTab = $('.sell', tabs);
      this.buyTab = $('.buy', tabs);
      this.sellAmount = $.id('sellAmount');
      this.buyAmount = $.id('buyAmount');
      this.sellExRate = $.id('sellExRate');
      this.buyExRate = $.id('buyExRate');
      this.enableRates();
      el = $('.container');
      market = el.textContent.match(/Order\ Book\ \(([A-Z]+)\/([A-Z]+)/);
      if (!(coin = market != null ? market[1] : void 0)) {
        return;
      }
      base = market[2];
      balances = $.id('elementDisplayBalances');
      currencies = balances.getElementsByTagName('div');
      for (i = _i = 0, _len = currencies.length; _i < _len; i = ++_i) {
        el = currencies[i];
        if (el.textContent === ("" + coin + ":")) {
          foundCoin = true;
          coin = currencies[++i];
          continue;
        }
        if (el.textContent === ("" + base + ":")) {
          foundBase = true;
          base = currencies[++i];
          continue;
        }
        if (foundCoin && foundBase) {
          break;
        }
      }
      if (!foundCoin && !foundBase) {
        return;
      }
      this.coin(coin);
      return this.base(base);
    },
    coin: function(el) {
      if (typeof el === 'string') {
        return;
      }
      el.style.cursor = 'pointer';
      return $.on(el, 'click', function() {
        if (!$.hasClass(CoinedUp.sellTab, 'active')) {
          return;
        }
        CoinedUp.sellAmount.value = this.textContent;
        return CoinedUp.sellAmount.onchange();
      });
    },
    base: function(el) {
      if (typeof el === 'string') {
        return;
      }
      el.style.cursor = 'pointer';
      return $.on(el, 'click', function() {
        if (!$.hasClass(CoinedUp.buyTab, 'active')) {
          return;
        }
        CoinedUp.buyAmount.value = (parseFloat(this.textContent) / parseFloat(CoinedUp.buyExRate.value)).toFixed(8);
        return CoinedUp.buyAmount.onchange();
      });
    },
    enableRates: function() {
      var orderBook;
      orderBook = $.id('elementDisplayOrderBook');
      $.off(orderBook, 'click', this.handleRates);
      $.on(orderBook, 'click', this.handleRates);
      $.off(orderBook, 'DOMSubtreeModified', this.enableRates);
      return $.on(orderBook, 'DOMSubtreeModified', this.enableRates);
    },
    handleRates: function(e) {
      var firstChild, target, update;
      target = e.target;
      firstChild = target.parentNode.firstChild;
      if (!parseFloat(firstChild.textContent)) {
        return;
      }
      update = $.hasClass(CoinedUp.sellTab, 'active') ? CoinedUp.sellExRate : CoinedUp.buyExRate;
      update.value = firstChild.textContent;
      return update.onchange();
    }
  };

  Main = {
    init: function() {
      var exchange, _i, _len, _ref;
      _ref = [Cryptsy, CoinedUp];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        exchange = _ref[_i];
        if (RegExp("" + exchange.name, "i").test(location.host)) {
          exchange.init();
        }
      }
    }
  };

  Main.init();

}).call(this);
