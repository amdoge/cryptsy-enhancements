What does it do?

- Enables editing of the `N BTC Available` field allowing to make orders more precise.

- Clicking on `Net Total (BTC)` in the sell form will copy that value to `N BTC Available`.

- Will update the tab title with the number of current open orders.

- Will play a sound once an order is completed (this doesn't work for small orders that are instantly executed, only for big ones).
